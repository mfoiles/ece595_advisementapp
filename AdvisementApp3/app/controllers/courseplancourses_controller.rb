class CourseplancoursesController < ApplicationController
  before_action :set_courseplancourse, only: [:show, :edit, :update, :destroy]

  # GET /courseplancourses
  # GET /courseplancourses.json
  def index
    @courseplancourses = Courseplancourse.all
  end

  # GET /courseplancourses/1
  # GET /courseplancourses/1.json
  def show
  end

  # GET /courseplancourses/new
  def new
    @courseplancourse = Courseplancourse.new
  end

  # GET /courseplancourses/1/edit
  def edit
  end

  # POST /courseplancourses
  # POST /courseplancourses.json
  def create
    @courseplancourse = Courseplancourse.new(courseplancourse_params)

    respond_to do |format|
      if @courseplancourse.save
        format.html { redirect_to @courseplancourse, notice: 'Courseplancourse was successfully created.' }
        format.json { render :show, status: :created, location: @courseplancourse }
      else
        format.html { render :new }
        format.json { render json: @courseplancourse.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /courseplancourses/1
  # PATCH/PUT /courseplancourses/1.json
  def update
    respond_to do |format|
      if @courseplancourse.update(courseplancourse_params)
        format.html { redirect_to @courseplancourse, notice: 'Courseplancourse was successfully updated.' }
        format.json { render :show, status: :ok, location: @courseplancourse }
      else
        format.html { render :edit }
        format.json { render json: @courseplancourse.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /courseplancourses/1
  # DELETE /courseplancourses/1.json
  def destroy
    @courseplancourse.destroy
    respond_to do |format|
      format.html { redirect_to courseplancourses_url, notice: 'Courseplancourse was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_courseplancourse
      @courseplancourse = Courseplancourse.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def courseplancourse_params
      params.require(:courseplancourse).permit(:courseplancourse_id, :course_id, :courseplan_id, :required_grade)
    end
end
