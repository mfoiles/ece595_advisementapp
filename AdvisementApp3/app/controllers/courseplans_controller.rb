class CourseplansController < ApplicationController
  before_action :set_courseplan, only: [:show, :edit, :update, :destroy]

  # GET /courseplans
  # GET /courseplans.json
  def index
    @courseplans = Courseplan.all
  end

  # GET /courseplans/1
  # GET /courseplans/1.json
  def show
  end

  # GET /courseplans/new
  def new
    @courseplan = Courseplan.new
  end

  # GET /courseplans/1/edit
  def edit
  end

  # POST /courseplans
  # POST /courseplans.json
  def create
    @courseplan = Courseplan.new(courseplan_params)

    respond_to do |format|
      if @courseplan.save
        format.html { redirect_to @courseplan, notice: 'Courseplan was successfully created.' }
        format.json { render :show, status: :created, location: @courseplan }
      else
        format.html { render :new }
        format.json { render json: @courseplan.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /courseplans/1
  # PATCH/PUT /courseplans/1.json
  def update
    respond_to do |format|
      if @courseplan.update(courseplan_params)
        format.html { redirect_to @courseplan, notice: 'Courseplan was successfully updated.' }
        format.json { render :show, status: :ok, location: @courseplan }
      else
        format.html { render :edit }
        format.json { render json: @courseplan.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /courseplans/1
  # DELETE /courseplans/1.json
  def destroy
    @courseplan.destroy
    respond_to do |format|
      format.html { redirect_to courseplans_url, notice: 'Courseplan was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_courseplan
      @courseplan = Courseplan.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def courseplan_params
      params.require(:courseplan).permit(:courseplan_id, :name, :department, :degree, :academic_year)
    end
end
