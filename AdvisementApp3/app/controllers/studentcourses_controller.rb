class StudentcoursesController < ApplicationController
  before_action :set_studentcourse, only: [:show, :edit, :update, :destroy]

  # GET /studentcourses
  # GET /studentcourses.json
  def index
    @studentcourses = Studentcourse.all
  end

  # GET /studentcourses/1
  # GET /studentcourses/1.json
  def show
  end

  # GET /studentcourses/new
  def new
    @studentcourse = Studentcourse.new
  end

  # GET /studentcourses/1/edit
  def edit
  end

  # POST /studentcourses
  # POST /studentcourses.json
  def create
    @studentcourse = Studentcourse.new(studentcourse_params)

    respond_to do |format|
      if @studentcourse.save
        format.html { redirect_to @studentcourse, notice: 'Studentcourse was successfully created.' }
        format.json { render :show, status: :created, location: @studentcourse }
      else
        format.html { render :new }
        format.json { render json: @studentcourse.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /studentcourses/1
  # PATCH/PUT /studentcourses/1.json
  def update
    respond_to do |format|
      if @studentcourse.update(studentcourse_params)
        format.html { redirect_to @studentcourse, notice: 'Studentcourse was successfully updated.' }
        format.json { render :show, status: :ok, location: @studentcourse }
      else
        format.html { render :edit }
        format.json { render json: @studentcourse.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /studentcourses/1
  # DELETE /studentcourses/1.json
  def destroy
    @studentcourse.destroy
    respond_to do |format|
      format.html { redirect_to studentcourses_url, notice: 'Studentcourse was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_studentcourse
      @studentcourse = Studentcourse.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def studentcourse_params
      params.require(:studentcourse).permit(:studentcourse_id, :student_id, :course_id, :received_grade, :date_completed)
    end
end
