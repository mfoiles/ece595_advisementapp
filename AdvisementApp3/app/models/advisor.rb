class Advisor < ActiveRecord::Base
  has_many :students
  has_many :appointments
  has_many :notes
end
