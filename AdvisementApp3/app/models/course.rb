class Course < ActiveRecord::Base
  has_many :courseplancourses
  has_many :courseplans, :through => :courseplancourses
  has_many :studentcourses
  has_many :students, :through => :studentcourses
end
