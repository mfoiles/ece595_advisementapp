class Courseplan < ActiveRecord::Base
  has_many :courses, :through => :courseplancourses
  has_many :courseplancourses
  has_many :students
end
