class Student < ActiveRecord::Base
  has_many :studentcourses
  has_many :courses, :through => :studentcourses
  belongs_to :courseplan
  belongs_to :advisor
  has_many :appointments
  has_many :notes
end
