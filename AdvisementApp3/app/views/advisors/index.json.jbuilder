json.array!(@advisors) do |advisor|
  json.extract! advisor, :id, :advisor_id, :name, :department, :office_location, :email, :phone
  json.url advisor_url(advisor, format: :json)
end
