json.array!(@appointments) do |appointment|
  json.extract! appointment, :id, :appointment_id, :appointment_date, :student_id, :advisor_id
  json.url appointment_url(appointment, format: :json)
end
