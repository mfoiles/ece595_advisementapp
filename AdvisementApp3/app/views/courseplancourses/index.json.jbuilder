json.array!(@courseplancourses) do |courseplancourse|
  json.extract! courseplancourse, :id, :courseplancourse_id, :course_id, :courseplan_id, :required_grade
  json.url courseplancourse_url(courseplancourse, format: :json)
end
