json.array!(@courseplans) do |courseplan|
  json.extract! courseplan, :id, :courseplan_id, :name, :department, :degree, :academic_year
  json.url courseplan_url(courseplan, format: :json)
end
