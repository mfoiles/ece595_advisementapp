json.array!(@courses) do |course|
  json.extract! course, :id, :course_id, :name, :code, :credit_hours, :min_grade, :description
  json.url course_url(course, format: :json)
end
