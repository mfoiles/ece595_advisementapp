json.array!(@notes) do |note|
  json.extract! note, :id, :note_id, :message, :date_posted, :student_id, :advisor_id
  json.url note_url(note, format: :json)
end
