json.array!(@studentcourses) do |studentcourse|
  json.extract! studentcourse, :id, :studentcourse_id, :student_id, :course_id, :received_grade, :date_completed
  json.url studentcourse_url(studentcourse, format: :json)
end
