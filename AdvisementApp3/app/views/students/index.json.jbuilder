json.array!(@students) do |student|
  json.extract! student, :id, :student_id, :name, :lobo_id, :academic_standing, :gpa, :expect_grad_date, :courseplan_id
  json.url student_url(student, format: :json)
end
