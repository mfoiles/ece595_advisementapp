class CreateAdvisors < ActiveRecord::Migration
  def change
    create_table :advisors do |t|
      t.integer :advisor_id
      t.string :name
      t.string :department
      t.string :office_location
      t.string :email
      t.string :phone

      t.timestamps
    end
  end
end
