class CreateCourses < ActiveRecord::Migration
  def change
    create_table :courses do |t|
      t.integer :course_id
      t.string :name
      t.string :code
      t.string :credit_hours
      t.string :min_grade
      t.text :description

      t.timestamps
    end
  end
end
