class CreateCourseplancourses < ActiveRecord::Migration
  def change
    create_table :courseplancourses do |t|
      t.integer :courseplancourse_id
      t.integer :course_id
      t.integer :courseplan_id
      t.string :required_grade

      t.timestamps
    end
  end
end
