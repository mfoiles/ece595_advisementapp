class CreateStudentcourses < ActiveRecord::Migration
  def change
    create_table :studentcourses do |t|
      t.string :studentcourse_id
      t.integer :student_id
      t.integer :course_id
      t.string :received_grade
      t.string :date_completed

      t.timestamps
    end
  end
end
