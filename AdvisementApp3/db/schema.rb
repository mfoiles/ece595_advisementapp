# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141115201935) do

  create_table "advisors", force: true do |t|
    t.integer  "advisor_id"
    t.string   "name"
    t.string   "department"
    t.string   "office_location"
    t.string   "email"
    t.string   "phone"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "appointments", force: true do |t|
    t.integer  "appointment_id"
    t.datetime "appointment_date"
    t.integer  "student_id"
    t.integer  "advisor_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "courseplancourses", force: true do |t|
    t.integer  "courseplancourse_id"
    t.integer  "course_id"
    t.integer  "courseplan_id"
    t.string   "required_grade"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "courseplans", force: true do |t|
    t.integer  "courseplan_id"
    t.string   "name"
    t.string   "department"
    t.string   "degree"
    t.string   "academic_year"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "courses", force: true do |t|
    t.integer  "course_id"
    t.string   "name"
    t.string   "code"
    t.string   "credit_hours"
    t.string   "min_grade"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "notes", force: true do |t|
    t.integer  "note_id"
    t.text     "message"
    t.datetime "date_posted"
    t.integer  "student_id"
    t.integer  "advisor_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "studentcourses", force: true do |t|
    t.string   "studentcourse_id"
    t.integer  "student_id"
    t.integer  "course_id"
    t.string   "received_grade"
    t.string   "date_completed"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "students", force: true do |t|
    t.integer  "student_id"
    t.string   "name"
    t.integer  "lobo_id"
    t.string   "academic_standing"
    t.float    "gpa"
    t.date     "expect_grad_date"
    t.integer  "courseplan_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
