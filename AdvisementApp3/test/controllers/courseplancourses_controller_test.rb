require 'test_helper'

class CourseplancoursesControllerTest < ActionController::TestCase
  setup do
    @courseplancourse = courseplancourses(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:courseplancourses)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create courseplancourse" do
    assert_difference('Courseplancourse.count') do
      post :create, courseplancourse: { course_id: @courseplancourse.course_id, courseplan_id: @courseplancourse.courseplan_id, courseplancourse_id: @courseplancourse.courseplancourse_id, required_grade: @courseplancourse.required_grade }
    end

    assert_redirected_to courseplancourse_path(assigns(:courseplancourse))
  end

  test "should show courseplancourse" do
    get :show, id: @courseplancourse
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @courseplancourse
    assert_response :success
  end

  test "should update courseplancourse" do
    patch :update, id: @courseplancourse, courseplancourse: { course_id: @courseplancourse.course_id, courseplan_id: @courseplancourse.courseplan_id, courseplancourse_id: @courseplancourse.courseplancourse_id, required_grade: @courseplancourse.required_grade }
    assert_redirected_to courseplancourse_path(assigns(:courseplancourse))
  end

  test "should destroy courseplancourse" do
    assert_difference('Courseplancourse.count', -1) do
      delete :destroy, id: @courseplancourse
    end

    assert_redirected_to courseplancourses_path
  end
end
