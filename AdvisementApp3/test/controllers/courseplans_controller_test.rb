require 'test_helper'

class CourseplansControllerTest < ActionController::TestCase
  setup do
    @courseplan = courseplans(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:courseplans)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create courseplan" do
    assert_difference('Courseplan.count') do
      post :create, courseplan: { academic_year: @courseplan.academic_year, courseplan_id: @courseplan.courseplan_id, degree: @courseplan.degree, department: @courseplan.department, name: @courseplan.name }
    end

    assert_redirected_to courseplan_path(assigns(:courseplan))
  end

  test "should show courseplan" do
    get :show, id: @courseplan
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @courseplan
    assert_response :success
  end

  test "should update courseplan" do
    patch :update, id: @courseplan, courseplan: { academic_year: @courseplan.academic_year, courseplan_id: @courseplan.courseplan_id, degree: @courseplan.degree, department: @courseplan.department, name: @courseplan.name }
    assert_redirected_to courseplan_path(assigns(:courseplan))
  end

  test "should destroy courseplan" do
    assert_difference('Courseplan.count', -1) do
      delete :destroy, id: @courseplan
    end

    assert_redirected_to courseplans_path
  end
end
