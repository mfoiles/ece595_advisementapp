require 'test_helper'

class StudentcoursesControllerTest < ActionController::TestCase
  setup do
    @studentcourse = studentcourses(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:studentcourses)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create studentcourse" do
    assert_difference('Studentcourse.count') do
      post :create, studentcourse: { course_id: @studentcourse.course_id, date_completed: @studentcourse.date_completed, received_grade: @studentcourse.received_grade, student_id: @studentcourse.student_id, studentcourse_id: @studentcourse.studentcourse_id }
    end

    assert_redirected_to studentcourse_path(assigns(:studentcourse))
  end

  test "should show studentcourse" do
    get :show, id: @studentcourse
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @studentcourse
    assert_response :success
  end

  test "should update studentcourse" do
    patch :update, id: @studentcourse, studentcourse: { course_id: @studentcourse.course_id, date_completed: @studentcourse.date_completed, received_grade: @studentcourse.received_grade, student_id: @studentcourse.student_id, studentcourse_id: @studentcourse.studentcourse_id }
    assert_redirected_to studentcourse_path(assigns(:studentcourse))
  end

  test "should destroy studentcourse" do
    assert_difference('Studentcourse.count', -1) do
      delete :destroy, id: @studentcourse
    end

    assert_redirected_to studentcourses_path
  end
end
