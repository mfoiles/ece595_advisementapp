class RequiredcoursesController < ApplicationController
  before_action :set_requiredcourse, only: [:show, :edit, :update, :destroy]

  # GET /requiredcourses
  # GET /requiredcourses.json
  def index
    @requiredcourses = Requiredcourse.all
  end

  # GET /requiredcourses/1
  # GET /requiredcourses/1.json
  def show
  end

  # GET /requiredcourses/new
  def new
    @requiredcourse = Requiredcourse.new
  end

  # GET /requiredcourses/1/edit
  def edit
  end

  # POST /requiredcourses
  # POST /requiredcourses.json
  def create
    @requiredcourse = Requiredcourse.new(requiredcourse_params)

    respond_to do |format|
      if @requiredcourse.save
        format.html { redirect_to @requiredcourse, notice: 'Requiredcourse was successfully created.' }
        format.json { render :show, status: :created, location: @requiredcourse }
      else
        format.html { render :new }
        format.json { render json: @requiredcourse.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /requiredcourses/1
  # PATCH/PUT /requiredcourses/1.json
  def update
    respond_to do |format|
      if @requiredcourse.update(requiredcourse_params)
        format.html { redirect_to @requiredcourse, notice: 'Requiredcourse was successfully updated.' }
        format.json { render :show, status: :ok, location: @requiredcourse }
      else
        format.html { render :edit }
        format.json { render json: @requiredcourse.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /requiredcourses/1
  # DELETE /requiredcourses/1.json
  def destroy
    @requiredcourse.destroy
    respond_to do |format|
      format.html { redirect_to requiredcourses_url, notice: 'Requiredcourse was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_requiredcourse
      @requiredcourse = Requiredcourse.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def requiredcourse_params
      params.require(:requiredcourse).permit(:requiredcourse_id, :courseplan_id, :name, :code, :credit_hours, :required_grade, :description)
    end
end
