class Appointment < ActiveRecord::Base
  belongs_to :student
  belongs_to :advisor
  #validates_presence_of :appointment_id
  validates_presence_of :appointment_date
  validates_presence_of :student_id
  validates_presence_of :advisor_id
  validates_presence_of :notes
end
