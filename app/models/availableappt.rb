class Availableappt < ActiveRecord::Base
  belongs_to :advisor
  validates_presence_of :advisor_id
  validates_presence_of :date
end
