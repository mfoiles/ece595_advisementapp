class Course < ActiveRecord::Base
  belongs_to :student
  validates_presence_of :student_id
  validates_presence_of :name
  validates_presence_of :code
  validates_presence_of :credit_hours
  validates_presence_of :grade
end
