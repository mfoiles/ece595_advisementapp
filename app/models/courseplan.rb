class Courseplan < ActiveRecord::Base
  belongs_to :student
  has_many :requiredcourses
  #validates_presence_of :student_id
  validates_presence_of :name
  validates_presence_of :department
  validates_presence_of :degree
  validates_presence_of :academic_year
end
