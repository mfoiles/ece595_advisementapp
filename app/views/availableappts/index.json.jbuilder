json.array!(@availableappts) do |availableappt|
  json.extract! availableappt, :id, :appt_id, :advisor_id, :date
  json.url availableappt_url(availableappt, format: :json)
end
