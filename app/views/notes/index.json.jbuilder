json.array!(@notes) do |note|
  json.extract! note, :id, :note_id, :student_id, :advisor_id, :message
  json.url note_url(note, format: :json)
end
