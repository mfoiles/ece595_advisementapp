class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.integer :appointment_id
      t.datetime :appointment_date
      t.integer :student_id
      t.integer :advisor_id
      t.text :notes

      t.timestamps
    end
  end
end
