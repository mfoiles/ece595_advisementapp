class CreateStudents < ActiveRecord::Migration
  def change
    create_table :students do |t|
      t.integer :student_id
      t.integer :advisor_id
      t.string :name
      t.integer :lobo_id
      t.string :academic_standing
      t.float :gpa
      t.date :expect_grad_date
      t.integer :courseplan_id

      t.timestamps
    end
  end
end
