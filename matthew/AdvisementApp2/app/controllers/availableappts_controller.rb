class AvailableapptsController < ApplicationController
  before_action :set_availableappt, only: [:show, :edit, :update, :destroy]

  # GET /availableappts
  # GET /availableappts.json
  def index
    @availableappts = Availableappt.all
  end

  # GET /availableappts/1
  # GET /availableappts/1.json
  def show
  end

  # GET /availableappts/new
  def new
    @availableappt = Availableappt.new
  end

  # GET /availableappts/1/edit
  def edit
  end

  # POST /availableappts
  # POST /availableappts.json
  def create
    @availableappt = Availableappt.new(availableappt_params)

    respond_to do |format|
      if @availableappt.save
        format.html { redirect_to @availableappt, notice: 'Availableappt was successfully created.' }
        format.json { render :show, status: :created, location: @availableappt }
      else
        format.html { render :new }
        format.json { render json: @availableappt.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /availableappts/1
  # PATCH/PUT /availableappts/1.json
  def update
    respond_to do |format|
      if @availableappt.update(availableappt_params)
        format.html { redirect_to @availableappt, notice: 'Availableappt was successfully updated.' }
        format.json { render :show, status: :ok, location: @availableappt }
      else
        format.html { render :edit }
        format.json { render json: @availableappt.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /availableappts/1
  # DELETE /availableappts/1.json
  def destroy
    @availableappt.destroy
    respond_to do |format|
      format.html { redirect_to availableappts_url, notice: 'Availableappt was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_availableappt
      @availableappt = Availableappt.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def availableappt_params
      params.require(:availableappt).permit(:appt_id, :advisor_id, :date)
    end
end
