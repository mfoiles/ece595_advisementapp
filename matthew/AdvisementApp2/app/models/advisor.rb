class Advisor < ActiveRecord::Base
  has_many :students
  has_many :appointments, dependent: :destroy
  has_many :availableappts, dependent: :destroy
end
