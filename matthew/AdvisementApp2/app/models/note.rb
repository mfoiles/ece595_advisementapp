class Note < ActiveRecord::Base
  belongs_to :student
  validates_presence_of :student_id
  validates_presence_of :advisor_id
  validates_presence_of :message
end
