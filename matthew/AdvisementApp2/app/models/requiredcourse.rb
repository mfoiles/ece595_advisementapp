class Requiredcourse < ActiveRecord::Base
  belongs_to :courseplan
  validates_presence_of :courseplan_id
  validates_presence_of :name
  validates_presence_of :code
  validates_presence_of :required_grade
  #validates_presence_of :description
end
