class Student < ActiveRecord::Base
  has_many :courses, dependent: :destroy
  has_many :notes, dependent: :destroy
  has_many :appointments, dependent: :destroy
  has_one :courseplan
  belongs_to :advisor
  validates_presence_of :name
  validates_presence_of :lobo_id
  validates_presence_of :advisor_id
end
