json.array!(@courses) do |course|
  json.extract! course, :id, :course_id, :student_id, :name, :code, :credit_hours, :grade
  json.url course_url(course, format: :json)
end
