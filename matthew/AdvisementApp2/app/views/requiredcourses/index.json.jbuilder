json.array!(@requiredcourses) do |requiredcourse|
  json.extract! requiredcourse, :id, :requiredcourse_id, :courseplan_id, :name, :code, :credit_hours, :required_grade, :description
  json.url requiredcourse_url(requiredcourse, format: :json)
end
