class CreateCourses < ActiveRecord::Migration
  def change
    create_table :courses do |t|
      t.integer :course_id
      t.integer :student_id
      t.string :name
      t.string :code
      t.string :credit_hours
      t.string :grade

      t.timestamps
    end
  end
end
