class CreateCourseplans < ActiveRecord::Migration
  def change
    create_table :courseplans do |t|
      t.integer :courseplan_id
      t.integer :student_id
      t.string :name
      t.string :department
      t.string :degree
      t.string :academic_year

      t.timestamps
    end
  end
end
