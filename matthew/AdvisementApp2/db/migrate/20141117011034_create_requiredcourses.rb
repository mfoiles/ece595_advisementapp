class CreateRequiredcourses < ActiveRecord::Migration
  def change
    create_table :requiredcourses do |t|
      t.integer :requiredcourse_id
      t.integer :courseplan_id
      t.string :name
      t.string :code
      t.string :credit_hours
      t.string :required_grade
      t.text :description

      t.timestamps
    end
  end
end
