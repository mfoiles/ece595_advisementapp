class CreateNotes < ActiveRecord::Migration
  def change
    create_table :notes do |t|
      t.integer :note_id
      t.integer :student_id
      t.integer :advisor_id
      t.text :message

      t.timestamps
    end
  end
end
