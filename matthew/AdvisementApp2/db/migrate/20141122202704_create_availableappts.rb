class CreateAvailableappts < ActiveRecord::Migration
  def change
    create_table :availableappts do |t|
      t.integer :appt_id
      t.integer :advisor_id
      t.datetime :date

      t.timestamps
    end
  end
end
