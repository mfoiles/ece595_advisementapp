# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141130001122) do

  create_table "advisors", force: true do |t|
    t.integer  "advisor_id"
    t.string   "name"
    t.string   "department"
    t.string   "office_location"
    t.string   "email"
    t.string   "phone"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "appointments", force: true do |t|
    t.integer  "appointment_id"
    t.datetime "appointment_date"
    t.integer  "student_id"
    t.integer  "advisor_id"
    t.text     "notes"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "availableappts", force: true do |t|
    t.integer  "appt_id"
    t.integer  "advisor_id"
    t.datetime "date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "courseplans", force: true do |t|
    t.integer  "courseplan_id"
    t.integer  "student_id"
    t.string   "name"
    t.string   "department"
    t.string   "degree"
    t.string   "academic_year"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "courses", force: true do |t|
    t.integer  "course_id"
    t.integer  "student_id"
    t.string   "name"
    t.string   "code"
    t.string   "credit_hours"
    t.string   "grade"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "notes", force: true do |t|
    t.integer  "note_id"
    t.integer  "student_id"
    t.integer  "advisor_id"
    t.text     "message"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "requiredcourses", force: true do |t|
    t.integer  "requiredcourse_id"
    t.integer  "courseplan_id"
    t.string   "name"
    t.string   "code"
    t.string   "credit_hours"
    t.string   "required_grade"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "students", force: true do |t|
    t.integer  "student_id"
    t.integer  "advisor_id"
    t.string   "name"
    t.integer  "lobo_id"
    t.string   "academic_standing"
    t.float    "gpa"
    t.date     "expect_grad_date"
    t.integer  "courseplan_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

end
