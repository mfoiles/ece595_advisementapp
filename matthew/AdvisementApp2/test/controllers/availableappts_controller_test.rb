require 'test_helper'

class AvailableapptsControllerTest < ActionController::TestCase
  setup do
    @availableappt = availableappts(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:availableappts)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create availableappt" do
    assert_difference('Availableappt.count') do
      post :create, availableappt: { advisor_id: @availableappt.advisor_id, appt_id: @availableappt.appt_id, date: @availableappt.date }
    end

    assert_redirected_to availableappt_path(assigns(:availableappt))
  end

  test "should show availableappt" do
    get :show, id: @availableappt
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @availableappt
    assert_response :success
  end

  test "should update availableappt" do
    patch :update, id: @availableappt, availableappt: { advisor_id: @availableappt.advisor_id, appt_id: @availableappt.appt_id, date: @availableappt.date }
    assert_redirected_to availableappt_path(assigns(:availableappt))
  end

  test "should destroy availableappt" do
    assert_difference('Availableappt.count', -1) do
      delete :destroy, id: @availableappt
    end

    assert_redirected_to availableappts_path
  end
end
