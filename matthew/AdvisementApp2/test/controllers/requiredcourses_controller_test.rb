require 'test_helper'

class RequiredcoursesControllerTest < ActionController::TestCase
  setup do
    @requiredcourse = requiredcourses(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:requiredcourses)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create requiredcourse" do
    assert_difference('Requiredcourse.count') do
      post :create, requiredcourse: { code: @requiredcourse.code, courseplan_id: @requiredcourse.courseplan_id, credit_hours: @requiredcourse.credit_hours, description: @requiredcourse.description, name: @requiredcourse.name, required_grade: @requiredcourse.required_grade, requiredcourse_id: @requiredcourse.requiredcourse_id }
    end

    assert_redirected_to requiredcourse_path(assigns(:requiredcourse))
  end

  test "should show requiredcourse" do
    get :show, id: @requiredcourse
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @requiredcourse
    assert_response :success
  end

  test "should update requiredcourse" do
    patch :update, id: @requiredcourse, requiredcourse: { code: @requiredcourse.code, courseplan_id: @requiredcourse.courseplan_id, credit_hours: @requiredcourse.credit_hours, description: @requiredcourse.description, name: @requiredcourse.name, required_grade: @requiredcourse.required_grade, requiredcourse_id: @requiredcourse.requiredcourse_id }
    assert_redirected_to requiredcourse_path(assigns(:requiredcourse))
  end

  test "should destroy requiredcourse" do
    assert_difference('Requiredcourse.count', -1) do
      delete :destroy, id: @requiredcourse
    end

    assert_redirected_to requiredcourses_path
  end
end
